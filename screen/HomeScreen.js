import React, {Component} from 'react';
import {Button,View} from 'react-native';
import {
    StackNavigator,
  } from 'react-navigation';
  

export default class HomeScreen extends React.Component {
    static navigationOptions = {
      title: 'Welcome',
    };
    render() {
      const { navigate } = this.props.navigation;
      return (
        <View>
          <Button
            title="Go to Jane's Details"
            onPress={() =>
              navigate('Details', { name: 'Jane' })
            }
          />
          <Button
            title="Go to SectionList"
            onPress={() =>
              navigate('SectionList')
            }
          />
          <Button
            title="Go to FetchExample"
            onPress={() =>
              navigate('FetchExample')
            }
          />
          <Button
            title="Go to Take Photo"
            onPress={() =>
              navigate('TakePhoto')
            }
          />
        </View>
        
      );
    }
  }