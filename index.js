/** @format */

import {AppRegistry} from 'react-native';

import {name as appName} from './app.json';
import {
    createStackNavigator,
  } from 'react-navigation';
  
import IScrolledDownAndWhatHappenedNextShockedMe from './component/IScrolledDownAndWhatHappenedNextShockedMe'
import SectionListScreen from './screen/SectionListScreen'
import FetchExampleScreen from './screen/FetchExampleScreen'
import HomeScreen from './screen/HomeScreen'
import TakePhotoScreen from './screen/TakePhotoScreen'
import DetailScreen from './screen/DetailScreen'

navigationOptions: ({ navigation }) => ({
  title: `${navigation.state.routeName}`,
  headerTintColor: 'white',
  headerStyle: { backgroundColor: 'black', borderWidth: 1, borderBottomColor: 'white' },
  headerTitleStyle: { color: 'white' }
})

const WeApp = createStackNavigator({
    Home : HomeScreen,
    Details: DetailScreen,
    Profile: {screen: SectionListScreen},
    FetchExample:FetchExampleScreen,
    SectionList:SectionListScreen,
    TakePhoto: TakePhotoScreen,
  },{
    initialRouteName: 'Home',
  });
// skip this line if using Create React Native App
AppRegistry.registerComponent(appName, () => WeApp);