/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput,Button,Alert} from 'react-native';
import FlexDirectionBasics from './component/XXComponent';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

// type Props = {};
export default class App extends Component {
  constructor(Props){
    super(Props);
    this.state = {personName:''};
  }

  _onPressButton() {
    Alert.alert('You tapped the button!')
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
        <Text style={styles.instructions }> What's your name</Text>
        <TextInput style={styles.InputStyle } placeholder="Type here to translate!" onChangeText={(personName) => this.setState({personName})}></TextInput>
        <Text style={styles.we }> OK , {this.state.personName}</Text>
        <Button
          onPress={this._onPressButton}
          title="Press Me"
          color="#841584"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFF00',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    margin:10
  },we: {
    color: '#cc00cc',
    fontSize:30
  },InputStyle: {
    color: '#cc00cc',
    fontSize:16,
    width:180,
    height:60
  }
});
